var React = require('react');
var ReactDOM = require('react-dom');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');
var Infinite = require('react-infinite');
var ajax = require('./ajax');


/*Reddit data variable*/
var savedposts = [];
var after;

/* Reddit data variables for next scroll */
var newURL = '';
var nextData = [];

var ListItem = React.createClass({
    addOrder: function() {
      this.props.addOrder({
        price:this.props.price,
        id: this.props.num,
        });
    },
    showMap: function() {
      this.props.showMap(this.props.num);
    },
    render: function() {
      if (this.props.mapState) {
        var map = (<div className="infinite-map-gen" tabIndex="0">
                    <img src="https://maps.googleapis.com/maps/api/staticmap?center=480%20Brant%20Street%20Burlington&amp;zoom=16&amp;size=300x300&amp;scale=1.50&amp;markers=red%7C43.327101,-79.8002722&amp;key=AIzaSyClWIHG25LLDXl9T37KKxcaPF26sUni6dc"/>
                  </div>);
      }
        return <div className="infinite-list-item">
          <div className="infinite-photo-wrapper" onMouseOver={this.showMap} onMouseOut={this.showMap}>
            <img className="infinite-photo" src={this.props.thumbnail}/>
            <div className="infinite-photo-blur"><img className="infinite-blurred-pic" src={this.props.thumbnail}/></div>
            <div className="infinite-heart"><i className="fa fa-heart-o"></i></div>
            <div className="infinite-photo-headline">
              <a className="infinite-photo-title" href={this.props.url}>{this.props.title}</a>
              <span className="infinite-price">${this.props.price}</span>
            </div>
          </div>
          <ReactCSSTransitionGroup transitionName="maptransition" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
            {map}
          </ReactCSSTransitionGroup>
            
          
          <div className="infinite-options">
            <ol className="infinite-options-list">
              <li className="infinite-options-list-item"><button className="infinite-options-list-item-button" onClick={this.addOrder}><i className="fa fa-plus-square"></i></button></li>
              <li className="infinite-options-list-item"><button className="infinite-options-list-item-button" onClick={this.showMap}><i className="fa fa-map-marker"></i></button></li>
              <li className="infinite-options-list-item"><button className="infinite-options-list-item-button"><i className="fa fa-share-alt-square"></i></button></li>
            </ol>
          </div>

        </div>;
    }
});

var App = React.createClass({
    getInitialState: function() {
        return {
            elements: [],
            order: {
            },
            total: 0,
            isInfiniteLoading: false
        }
    },

    componentDidMount : function() {
      var that = this;
      ajax({
        url: 'https://www.reddit.com/r/foodporn/.json?limit=5',
        success: function(data){
          savedposts = JSON.parse(data).data.children.map(that.parseData);
          after = JSON.parse(data).data.after;  

          ajax.getJSON('https://www.reddit.com/r/foodporn/.json?limit=5&after=' + after, function(data) {
            nextData = data.data.children.map(that.parseData);
            after = data.data.after;
          });
        },
        complete: function() {
          that.setState({elements : that.buildRedditElements(0, 5, 'initial')});
        },
        async: false
      });
    },

    parseData : function(data) {
      var thumbnail;
      try {
        thumbnail = data.data.thumbnail;
        //thumbnail = data.data.preview.images[0].source.url;
      } catch (e) {
        thumbnail = data.data.thumbnail;
      }

      function convertPrice(num) {
        var digits = num.toString().length - 1;
        var numObj = (num / (Math.pow(10, digits))) + 5;
        return numObj.toFixed(2);
      }

      return {
        title : data.data.title.slice(0, 35) + '...',
        thumbnail : thumbnail,
        url : data.data.url,
        price : convertPrice(data.data.score),
        showMap : false
      };
    },

    buildRedditElements : function(start, end, initial) {
      var that = this;
      var limitCount = end - start;

      if (initial) {
        nextData = savedposts;
      } else {
        newURL = 'https://www.reddit.com/r/foodporn/.json?limit=' + limitCount + '&after=' + after;

        ajax.getJSON(newURL, function(data) {
          nextData = data.data.children.map(that.parseData);
          after = data.data.after;
        });
      }
      
      var posts = nextData;
      var postCount = 0;

      var counter = start - 1;
      /*
      posts = nextData.map(function(object) {
                        counter++;
                        return (<ListItem
                                  key={counter}
                                  num={counter}
                                  title={object.title}
                                  thumbnail={object.thumbnail}
                                  url={object.url}
                                  price={object.price}
                                  />);
                      });
      */

      return posts;
    },
    makeList: function(array) {
      var processed = [];
      for (var i = 0; i < array.length; i++) {
        processed.push( <ListItem
                          key={i}
                          num={i}
                          title={array[i].title}
                          thumbnail={array[i].thumbnail}
                          url={array[i].url}
                          price={array[i].price}
                          addOrder={this.addOrder}
                          showMap={this.showMap}
                          mapState={array[i].showMap}
                          />
          );
      }
      return processed;
    },
    addOrder: function(object) {      
      this.state.order[object.id] = this.state.order[object.id] + 1 || 1;

      this.setState({
        order: this.state.order
      })

      this.calcTotal();
    },
    calcTotal: function() {
      var that = this;

      //console.log(Object.keys(this.state.order));
      console.log(Object.keys(that.state.order));
      var totalCost = Object.keys(that.state.order).map(function(data) {
          var order = that.state.order;
          var quantity = order[data];
          var price = Number(that.state.elements[data].price);
          var total = price * quantity;
          return total;
      });

        console.log(totalCost);

        var finalSum = totalCost.reduce(function(prev, current) {
          return prev + current;
        })
        console.log('total amount is ' + finalSum);
        this.setState({
          total: finalSum.toFixed(2)
        });
    },

    showMap: function(num) {
      var setMap = this.state.elements[num].showMap ? false : true;
      this.state.elements[num].showMap = setMap;
      this.setState({
        elements: this.state.elements
      });
      console.log(this.state.elements[num].showMap);
    },

    handleInfiniteLoad: function() {
        var that = this;
        
        this.setState({
            isInfiniteLoading: true
        });
        setTimeout(function() {
            var elemLength = that.state.elements.length,
                newElements = that.buildRedditElements(elemLength, elemLength + 5);
            that.setState({
                isInfiniteLoading: false,
                elements: that.state.elements.concat(newElements)
            });
        }, 2500);
    },

    elementInfiniteLoad: function() {
        return <div className="infinite-list-load">
            Loading...
        </div>;
    },

    render: function() {
        

        return (

          <div>
            <Infinite elementHeight={300}
                           useWindowAsScrollContainer={true}
                
                           infiniteLoadBeginEdgeOffset={200}
                           onInfiniteLoad={this.handleInfiniteLoad}
                           loadingSpinnerDelegate={this.elementInfiniteLoad()}
                           isInfiniteLoading={this.state.isInfiniteLoading}
                           >
              {this.makeList(this.state.elements)}
            </Infinite>
            <Order total={this.state.total} orderList={this.state.order} menuItems={this.state.elements}/>
          </div>
          );



       
    }
});

var Order = React.createClass({
  renderOrderList: function(object) {
      return (<p key={object}>{this.props.orderList[object]}x{this.props.menuItems[object].title}</p>)
  },
  render: function() {
    var items = Object.keys(this.props.orderList).map(this.renderOrderList);
    return (
      <div className="infinite-order">
        <p>order page goes here</p>
        <ReactCSSTransitionGroup transitionName="example" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
          {items}
        </ReactCSSTransitionGroup>
        <p>Total cost is...$
          <ReactCSSTransitionGroup transitionName="orderTotal" transitionEnterTimeout={250} transitionLeaveTimeout={250}>
            <span className="orderTotalPos" key={this.props.total}>{this.props.total}</span>
          </ReactCSSTransitionGroup>
        </p>
      </div>
      )
  }
})



ReactDOM.render(<App/>, document.querySelector('#menu-container'));
